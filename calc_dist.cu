/*
 * Proj 3-2 SKELETON
 */

#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime.h>
#include <cutil.h>
#include "utils.h"

/* Does a horizontal flip of the array arr */
__global__ void flip_horizontalKernel(float *arr, float *out, int width) {
	/* YOU MAY WISH TO IMPLEMENT THIS */
	unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
	unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;
	if (x < width && y < width) {
		out[y * width + x] = arr[width - x - 1 + (y * width)];
	}
}

void flip_horizontal(float *arr, float *out, int width) {
	unsigned int dim = 16;
	dim3 dim_threads_per_block(dim, dim, 1);
	dim3 dim_blocks_per_grid(width / dim_threads_per_block.x,
			width / dim_threads_per_block.y);
	flip_horizontalKernel<<<dim_blocks_per_grid, dim_threads_per_block>>>(arr,
			out, width);
	cudaThreadSynchronize();
	CUT_CHECK_ERROR("");
}

/* Transposes the square array ARR. */
__global__ void transposeKernel(float *arr, float *out, int width) {
	/* YOU MAY WISH TO IMPLEMENT THIS */
	unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
	unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;
	if (x < width && y < width) {
		out[y * width + x] = arr[x * width + y];
	}
}

void transpose(float *arr, float *out, int width) {
	/* YOU MAY WISH TO IMPLEMENT THIS */
	unsigned int dim = 16;
	dim3 dim_threads_per_block(dim, dim, 1);
	dim3 dim_blocks_per_grid(width / dim_threads_per_block.x,
			width / dim_threads_per_block.y);
	transposeKernel<<<dim_blocks_per_grid, dim_threads_per_block>>>(arr, out,
			width);
	cudaThreadSynchronize();
	CUT_CHECK_ERROR("");
}

__global__ void reduction_kernel(float *gpu_result, int t_width, unsigned int level) {
	unsigned int len = t_width * t_width;
	unsigned int offset = len / (level * 2);
	unsigned int thisThreadIndex = (blockDim.x * blockIdx.x + threadIdx.x);
	if (thisThreadIndex < offset) {
		gpu_result[thisThreadIndex] += gpu_result[thisThreadIndex + offset];
	}
}

__global__ void calc_dist_kernel(float *gpu_result, float *image, float *temp, int i_width, int t_width,
		int x_offset, int y_offset){
	unsigned int x = ((blockIdx.x * blockDim.x) + threadIdx.x);
	unsigned int y = ((blockIdx.y * blockDim.y) + threadIdx.y);
	unsigned int index = y * t_width + x;
	gpu_result[index] = (temp[index] - image[(y + y_offset) * i_width + x + x_offset]) * (temp[index] - image[(y + y_offset) * i_width + x + x_offset]);
}

float calc_dist_reduction(float *gpu_result, float *image, float *temp, int i_width, int t_width,
		int x_offset, int y_offset) {
	unsigned int level = 1;
	unsigned int len = t_width * t_width;
	float dist = 0.0;
	unsigned int dim = 16;
	unsigned int gridX = t_width / dim;
	unsigned int gridY = t_width / dim;

	// squared Euclidean distance, save into gpu_result
	dim3 dim_threads_per_block(dim, dim, 1);
	dim3 dim_blocks_per_grid(gridX, gridY);
	calc_dist_kernel<<<dim_blocks_per_grid, dim_threads_per_block>>>(gpu_result, image, temp, i_width, t_width, x_offset, y_offset);
	cudaThreadSynchronize();
	CUT_CHECK_ERROR("");

	// do the reduction in gpu_result
	unsigned int threads_per_block = 512;
	unsigned int blocks_per_grid = (len / threads_per_block) + 1;
	while(level != len){
		dim3 dim_threads_per_block(threads_per_block, 1, 1);
		dim3 dim_blocks_per_grid(blocks_per_grid, 1);
		reduction_kernel<<<dim_blocks_per_grid, dim_threads_per_block>>>(gpu_result, t_width, level);
		cudaThreadSynchronize();
		CUT_CHECK_ERROR("");

		level *= 2;
		blocks_per_grid = (blocks_per_grid / 2) + 1;
	}

	// copy the first element in gpu_result
	CUDA_SAFE_CALL(cudaMemcpy(&dist, gpu_result, sizeof(float), cudaMemcpyDeviceToHost));

	return dist;
}

float calc_dist_loop(float *image, int i_width, int i_height, float *temp,
		int t_width) {
	float min_dist = FLT_MAX;
	float *gpu_result = NULL;
	CUDA_SAFE_CALL(cudaMalloc(&gpu_result, t_width * t_width * sizeof(float)));

	for (int i = 0; i < (i_width - t_width + 1); i++) {
		for (int j = 0; j < (i_height - t_width + 1); j++) {
			float min = calc_dist_reduction(gpu_result, image, temp, i_width, t_width, i, j);
			if (min < min_dist) {
				min_dist = min;
			}
		}
	}
	CUDA_SAFE_CALL(cudaFree(gpu_result));
	return min_dist;
}

/* Returns the squared Euclidean distance between TEMPLATE and IMAGE. The size of IMAGE
 * is I_WIDTH * I_HEIGHT, while TEMPLATE is square with side length T_WIDTH. The template
 * image should be flipped, rotated, and translated across IMAGE.
 */
float calc_min_dist(float *image, int i_width, int i_height, float *temp,
		int t_width) {
	// float* image and float* temp are pointers to GPU addressible memory
	// You MAY NOT copy this data back to CPU addressible memory and you MAY
	// NOT perform any computation using values from image or temp on the CPU.

	/* YOUR CODE HERE */
	float min_dist = FLT_MAX;
	float min[8]; // for saving eight minimun result
	float *gpu_out;
	unsigned int len = t_width * t_width;
	unsigned int arraySize = len * sizeof(float);
	// allocate memory in GPU
	CUDA_SAFE_CALL(cudaMalloc(&gpu_out, arraySize));

	// rotating and iterating every possibility
	min[0] = calc_dist_loop(image, i_width, i_height, temp, t_width);
	transpose(temp, gpu_out, t_width);

	min[1] = calc_dist_loop(image, i_width, i_height, gpu_out, t_width);
	flip_horizontal(gpu_out, temp, t_width);

	min[2] = calc_dist_loop(image, i_width, i_height, temp, t_width);
	transpose(temp, gpu_out, t_width);

	min[3] = calc_dist_loop(image, i_width, i_height, gpu_out, t_width);
	flip_horizontal(gpu_out, temp, t_width);

	min[4] = calc_dist_loop(image, i_width, i_height, temp, t_width);
	transpose(temp, gpu_out, t_width);

	min[5] = calc_dist_loop(image, i_width, i_height, gpu_out, t_width);
	flip_horizontal(gpu_out, temp, t_width);

	min[6] = calc_dist_loop(image, i_width, i_height, temp, t_width);
	transpose(temp, gpu_out, t_width);

	min[7] = calc_dist_loop(image, i_width, i_height, gpu_out, t_width);
	flip_horizontal(gpu_out, temp, t_width);

	for (int i = 0; i < 8; i++) {
		if (min[i] < min_dist) {
			min_dist = min[i];
		}
	}
	CUDA_SAFE_CALL(cudaFree(gpu_out));
	return min_dist;
}
